import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "characters_project.settings")
django.setup()

from characters_rest.models import Character

def poll():
	while True:
		print('Characters poller polling for data')
		try:
			nameresponse = requests.get("https://api.genshin.dev/characters")
			namecontent = json.loads(nameresponse.content)
			for n in namecontent:
				response = requests.get(f"https://api.genshin.dev/characters/{n}")
				content = json.loads(response.content)
				Character.objects.update_or_create(
					name= content["name"],
					element= content["vision"],
					defaults={
							"icon": f"https://api.genshin.dev/characters/{n}/icon",
							"picture": f"https://api.genshin.dev/characters/{n}/portrait",
							"banner": f"https://api.genshin.dev/characters/{n}/gacha-card/"
					}
				)
			print("success")
		except Exception as e:
			print(e, file=sys.stderr)
		time.sleep(60)


if __name__ == "__main__":
		poll()