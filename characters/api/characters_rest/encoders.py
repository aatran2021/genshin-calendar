from common.json import ModelEncoder

from .models import (
    Character,
    # WeeklyItemVO,
    # BookVO,
    # BossItemVO,
    # MobItemVO,
    # LocalItemVO
    )

class CharacterEncoder(ModelEncoder):
    model = Character
    properties = [
        "name",
        "element",
        "icon",
        "picture",
        "banner",
        # "weekly_item",
        # "book",
        # "boss_item",
        # "mob_item",
        # "local_item"
    ]

# class WeeklyItemVOEncoder(ModelEncoder):
#     model = WeeklyItemVO
#     properties = [
#         "name",
#         "import_href"
#     ]

# class BookVOEncoder(ModelEncoder):
#     model = BookVO
#     properties = [
#         "name",
#         "import_href"
#     ]

# class BossItemVOEncoder(ModelEncoder):
#     model = BossItemVO
#     properties = [
#         "name",
#         "import_href"
#     ]

# class MobItemVOEncoder(ModelEncoder):
#     model = MobItemVO
#     properties = [
#         "name",
#         "import_href"
#     ]

# class LocalItemVOEncoder(ModelEncoder):
#     model = LocalItemVO
#     properties = [
#         "name",
#         "import_href"
#     ]
