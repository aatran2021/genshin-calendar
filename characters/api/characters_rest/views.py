from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
# from .acls import get_photo


from .models import (
    Character,
    # WeeklyItemVO,
    # BookVO,
    # BossItemVO,
    # MobItemVO,
    # LocalItemVO
    )
from .encoders import (
    CharacterEncoder,
    # WeeklyItemVOEncoder,
    # BookVOEncoder,
    # BossItemVOEncoder,
    # MobItemVOEncoder,
    # LocalItemVOEncoder
    )

@require_http_methods(["GET", "POST"])
def api_characters(request):
    if request.method == "GET":
        characters = Character.objects.all()
        return JsonResponse(
            {"characters": characters},
            encoder=CharacterEncoder,
        )
    # else:
    #     try:
    #         content = json.loads(request.body)
    #         model_id = content["model_id"]
    #         model = VehicleModel.objects.get(pk=model_id)
    #         content["model"] = model
    #         auto = WeeklyItem.objects.create(**content)
    #         return JsonResponse(
    #             auto,
    #             encoder=WeeklyItemVOEncoder,
    #             safe=False,
    #         )
    #     except:
    #         response = JsonResponse(
    #             {"message": "Could not create the weekly item"}
    #         )
    #         response.status_code = 400
    #         return response


# @require_http_methods(["DELETE", "GET", "PUT"])
# def api_automobile(request, vin):
#     if request.method == "GET":
#         try:
#             auto = WeeklyItem.objects.get(vin=vin)
#             return JsonResponse(
#                 auto,
#                 encoder=AutomobileEncoder,
#                 safe=False
#             )
#         except WeeklyItem.DoesNotExist:
#             response = JsonResponse({"message": "Does not exist"})
#             response.status_code = 404
#             return response
#     elif request.method == "DELETE":
#         try:
#             auto = WeeklyItem.objects.get(vin=vin)
#             auto.delete()
#             return JsonResponse(
#                 auto,
#                 encoder=AutomobileEncoder,
#                 safe=False,
#             )
#         except WeeklyItem.DoesNotExist:
#             return JsonResponse({"message": "Does not exist"})
#     else: # PUT
#         try:
#             content = json.loads(request.body)
#             auto = WeeklyItem.objects.get(vin=vin)

#             props = ["color", "year", "sold"]
#             for prop in props:
#                 if prop in content:
#                     setattr(auto, prop, content[prop])
#             auto.save()
#             return JsonResponse(
#                 auto,
#                 encoder=AutomobileEncoder,
#                 safe=False,
#             )
#         except WeeklyItem.DoesNotExist:
#             response = JsonResponse({"message": "Does not exist"})
#             response.status_code = 404
#             return response


# @require_http_methods(["GET", "POST"])
# def api_weeklyitems(request):
#     if request.method == "GET":
#         weeklyitems = WeeklyItem.objects.all()
#         return JsonResponse(
#             {"weeklyitems": weeklyitems},
#             encoder=WeeklyItemVOEncoder,
#         )
#     else:
#         try:
#             content = json.loads(request.body)
#             model_id = content["model_id"]
#             model = VehicleModel.objects.get(pk=model_id)
#             content["model"] = model
#             auto = WeeklyItem.objects.create(**content)
#             return JsonResponse(
#                 auto,
#                 encoder=WeeklyItemVOEncoder,
#                 safe=False,
#             )
#         except:
#             response = JsonResponse(
#                 {"message": "Could not create the weekly item"}
#             )
#             response.status_code = 400
#             return response


# @require_http_methods(["DELETE", "GET", "PUT"])
# def api_automobile(request, vin):
#     if request.method == "GET":
#         try:
#             auto = WeeklyItem.objects.get(vin=vin)
#             return JsonResponse(
#                 auto,
#                 encoder=AutomobileEncoder,
#                 safe=False
#             )
#         except WeeklyItem.DoesNotExist:
#             response = JsonResponse({"message": "Does not exist"})
#             response.status_code = 404
#             return response
#     elif request.method == "DELETE":
#         try:
#             auto = WeeklyItem.objects.get(vin=vin)
#             auto.delete()
#             return JsonResponse(
#                 auto,
#                 encoder=AutomobileEncoder,
#                 safe=False,
#             )
#         except WeeklyItem.DoesNotExist:
#             return JsonResponse({"message": "Does not exist"})
#     else: # PUT
#         try:
#             content = json.loads(request.body)
#             auto = WeeklyItem.objects.get(vin=vin)

#             props = ["color", "year", "sold"]
#             for prop in props:
#                 if prop in content:
#                     setattr(auto, prop, content[prop])
#             auto.save()
#             return JsonResponse(
#                 auto,
#                 encoder=AutomobileEncoder,
#                 safe=False,
#             )
#         except WeeklyItem.DoesNotExist:
#             response = JsonResponse({"message": "Does not exist"})
#             response.status_code = 404
#             return response
