from django.db import models

# class WeeklyItemVO(models.Model):
#     import_href = models.CharField(max_length=200, unique=True)  
#     name = models.CharField(max_length=150)

#     def __str__(self):
#         return f"{self.vin}"

# class BookVO(models.Model):
#     import_href = models.CharField(max_length=200, unique=True)  
#     name = models.CharField(max_length=150)

#     def __str__(self):
#         return f"{self.vin}"

# class BossItemVO(models.Model):
#     import_href = models.CharField(max_length=200, unique=True)  
#     name = models.CharField(max_length=150)

#     def __str__(self):
#         return f"{self.vin}"

# class MobItemVO(models.Model):
#     import_href = models.CharField(max_length=200, unique=True)  
#     name = models.CharField(max_length=150)

#     def __str__(self):
#         return f"{self.vin}"

# class LocalItemVO(models.Model):
#     import_href = models.CharField(max_length=200, unique=True)  
#     name = models.CharField(max_length=150)

#     def __str__(self):
#         return f"{self.vin}"

class Character(models.Model):
    name = models.CharField(max_length=150)
    element = models.CharField(max_length=150)
    # image urls for character
    icon = models.URLField(max_length=250, null=True)
    picture = models.URLField(max_length=250, null=True)
    banner = models.URLField(max_length=250, null=True)
    # ascension and talent materials
    # weekly_item = models.ForeignKey(
    #     WeeklyItemVO,
    #     on_delete=models.CASCADE,
    #     related_name="character"
    # )

    # book = models.ForeignKey(
    #     BookVO,
    #     on_delete=models.CASCADE,
    #     related_name="character"
    # )

    # boss_item = models.ForeignKey(
    #     BossItemVO,
    #     on_delete=models.CASCADE,
    #     related_name="character"
    # )

    # mob_item = models.ForeignKey(
    #     MobItemVO,
    #     on_delete=models.CASCADE,
    #     related_name="character"
    # )   
    
    # local_item = models.ForeignKey(
    #     LocalItemVO,
    #     on_delete=models.CASCADE,
    #     related_name="character"
    # )    

    def __str__(self):
        return self.name + " - " + self.element
