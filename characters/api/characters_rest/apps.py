from django.apps import AppConfig


class CharactersRestConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'characters_rest'
