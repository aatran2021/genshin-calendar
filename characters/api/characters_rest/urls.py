from django.urls import path

from .views import api_characters

urlpatterns = [
    path("characters/", api_characters, name="api_characters")
]
