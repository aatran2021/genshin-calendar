import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "talents_project.settings")
django.setup()

from talents_rest.models import Book
# from talents_rest.weekliesdatabase import 

def poll():
	while True:
		print('Talents poller polling for data')
		try:
			response = requests.get("https://api.genshin.dev/materials/talent-book")
			content = json.loads(response.content)
			for item in content:
				print(item)
				Book.objects.update_or_create(
					name= item,
					domain= content[item]["source"],
					availability= content[item]["availability"],
					defaults={
					"characters": content[item]["characters"]
					}
				)
			print("success")
		except Exception as e:
			print(e, file=sys.stderr)
		time.sleep(60)


if __name__ == "__main__":
		poll()