from django.apps import AppConfig


class TalentsRestConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'talents_rest'
