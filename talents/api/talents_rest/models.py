from django.db import models
from django.contrib.postgres.fields import ArrayField


class Book(models.Model):
    name = models.CharField(max_length=200)
    domain = models.CharField(max_length=200)
    availability = ArrayField(models.CharField(max_length=200))
    characters = ArrayField(models.CharField(max_length=550))
    
    def __str__(self):
        return self.name