# Generated by Django 4.2 on 2023-05-06 23:03

import django.contrib.postgres.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('talents_rest', '0002_alter_book_characters'),
    ]

    operations = [
        migrations.AlterField(
            model_name='book',
            name='availability',
            field=django.contrib.postgres.fields.ArrayField(base_field=models.CharField(max_length=200), size=None),
        ),
        migrations.AlterField(
            model_name='book',
            name='domain',
            field=models.CharField(max_length=200),
        ),
        migrations.AlterField(
            model_name='book',
            name='name',
            field=models.CharField(max_length=200),
        ),
    ]
