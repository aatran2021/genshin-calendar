from django.apps import AppConfig


class RegularsRestConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'regulars_rest'
