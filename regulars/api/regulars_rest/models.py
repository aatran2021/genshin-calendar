from django.db import models
from django.contrib.postgres.fields import ArrayField


class BossItem(models.Model):
    name = models.CharField(max_length=200)
    boss = models.CharField(max_length=200)
    characters = ArrayField(models.CharField(max_length=550))
    def __str__(self):
        return self.name
    

class MobItem(models.Model):
    name = models.CharField(max_length=200)
    mob = models.CharField(max_length=200)
    characters = ArrayField(models.CharField(max_length=550))
    def __str__(self):
        return self.name    

class LocalItem(models.Model):
    name = models.CharField(max_length=200)
    location = models.CharField(max_length=200)
    characters = ArrayField(models.CharField(max_length=550))
    def __str__(self):
        return self.name 