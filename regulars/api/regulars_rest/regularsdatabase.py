# boss drops

bossdrops = {
  "hurricane-seed": {
    "name": "Hurricane Seed",
    "source": "Anemo Hypostasis",
    "characters": ["jean", "sucrose", "venti"]
  },
  "hoarfrost-core": {
    "name": "Hoarfrost Core",
    "source": "Cryo Regisvine",
    "characters": ["chongyun", "diona", "ganyu", "kaeya", "qiqi", "rosaria"]
  },
  "lightning-prism": {
    "name": "Lightning Prism",
    "source": "Electro Hypostasis",
    "characters": ["beidou", "fischl", "keqing", "lisa", "razor"]
  },
  "basalt-pillar": {
    "name": "Basalt Pillar",
    "source": "Geo Hypostasis",
    "characters": ["albedo", "ningguang", "noelle", "zhongli"]
  },
  "cleansing-heart": {
    "name": "Cleansing Heart",
    "source": "Oceanid",
    "characters": ["barbara", "mona", "tartaglia", "xingqiu"]
  },
  "everflame-seed": {
    "name": "Everflame Seed",
    "source": "Pyro Regisvine",
    "characters": ["amber", "bennett", "diluc", "klee", "xiangling", "Xinyan"]
  },
  "juvenile-jade": {
    "name": "Juvenile Jade",
    "source": "Primo Geovishap",
    "characters": ["hu-tao", "xiao", "yanfei"]
  },
  "crystalline-bloom": {
    "name": "Crystalline Bloom",
    "source": "Cryo Hypostasis",
    "characters": ["aloy", "eula"]
  },
  "marionette-core": {
    "name": "Marionette Core",
    "source": "Maguu Kenki",
    "characters": ["kazuha", "sayu"]
  },
  "perpetual-heart": {
    "name": "Perpetual Heart",
    "source": "Perpetual Mechanical Array",
    "characters": ["gorou", "ayaka"]
  },
  "smoldering-pearl": {
    "name": "Smoldering Pearl",
    "source": "Pyro Hypostasis",
    "characters": ["thoma", "yoimiya"]
  },
  "dew-of-repudiation": {
    "name": "Dew of Repudiation",
    "source": "Hydro Hypostasis",
    "characters": ["kokomi"]
  },
  "storm-beads": {
    "name": "Storm Beads",
    "source": "Thunder Manifestation",
    "characters": ["sara", "raiden"]
  },
  "riftborn-regalia": {
    "name": "Riftborn Regalia",
    "source": "Golden Wolflord",
    "characters": ["arataki-itto", "yun-jin"]
  },
  "dragonheir-s-false-fin": {
    "name": "Dragonheir's False Fin",
    "source": "Coral Defenders",
    "characters": ["shenhe", "yae-miko"]
  }
}

# monster drops

drops = {
  "slime": {
    "characters": ["lisa", "venti", "xiangling", "zhongli"],
    "items": [
      {
        "id": "slime-condensate",
        "name": "Slime Condensate",
        "rarity": 1
      },
      {
        "id": "slime-secretions",
        "name": "Slime Secretions",
        "rarity": 2
      },
      {
        "id": "slime-concentrate",
        "name": "Slime Concentrate",
        "rarity": 3
      }
    ],
    "sources": ["Slime"]
  },
  "hilichurl-masks": {
    "characters": ["jean", "noelle", "razor", "chongyun", "xingqiu"],
    "items": [
      {
        "id": "damaged-mask",
        "name": "Damaged Mask",
        "rarity": 1
      },
      {
        "id": "stained-mask",
        "name": "Stained Mask",
        "rarity": 2
      },
      {
        "id": "ominous-mask",
        "name": "Ominous Mask",
        "rarity": 3
      }
    ],
    "sources": ["Hilichurl", "Samachurl", "Mitachurl", "Lawachurl"]
  },
  "hilichurl-arrowheads": {
    "characters": ["amber", "diona", "fischl"],
    "items": [
      {
        "id": "firm-arrowhead",
        "name": "Firm Arrowhead",
        "rarity": 1
      },
      {
        "id": "sharp-arrowhead",
        "name": "Sharp Arrowhead",
        "rarity": 2
      },
      {
        "id": "weathered-arrowhead",
        "name": "Weathered Arrowhead",
        "rarity": 3
      }
    ],
    "sources": ["Hilichurl Archer"]
  },
  "samachurl-scrolls": {
    "characters": [
      "albedo",
      "barbara",
      "klee",
      "qiqi",
      "traveler-anemo",
      "traveler-geo"
    ],
    "items": [
      {
        "id": "divining-scroll",
        "name": "Divining Scroll",
        "rarity": 1
      },
      {
        "id": "sealed-scroll",
        "name": "Sealed Scroll",
        "rarity": 2
      },
      {
        "id": "forbidden-curse-scroll",
        "name": "Forbidden Curse Scroll",
        "rarity": 3
      }
    ],
    "sources": ["Samachurl"]
  },
  "treasure-hoarder-insignias": {
    "characters": ["kaeya", "bennett", "beidou", "xinyan"],
    "items": [
      {
        "id": "treasure-hoarder-insignia",
        "name": "Treasure Hoarder Insignia",
        "rarity": 1
      },
      {
        "id": "silver-raven-insignia",
        "name": "Silver Raven Insignia",
        "rarity": 2
      },
      {
        "id": "golden-raven-insignia",
        "name": "Golden Raven Insignia",
        "rarity": 3
      }
    ],
    "sources": ["Treasure Hoarder"]
  },
  "fatui-insignias": {
    "characters": ["diluc", "ningguang", "tartaglia"],
    "items": [
      {
        "id": "recruit's-insignia",
        "name": "Recruit's Insignia",
        "rarity": 1
      },
      {
        "id": "sergeant's-insignia",
        "name": "Sergeant's Insignia",
        "rarity": 2
      },
      {
        "id": "lieutenant's-insignia",
        "name": "Lieutenant's Insignia",
        "rarity": 3
      }
    ],
    "sources": ["Fatui Skirmisher", "Fatui Cicin Mage", "Fatui Agent"]
  },
  "whopperflower-nectar": {
    "characters": ["mona", "sucrose", "keqing"],
    "items": [
      {
        "id": "whopperflower-nectar",
        "name": "Whopperflower Nectar",
        "rarity": 1
      },
      {
        "id": "shimmering-nectar",
        "name": "Shimmering Nectar",
        "rarity": 2
      },
      {
        "id": "energy-nectar",
        "name": "Energy Nectar",
        "rarity": 3
      }
    ],
    "sources": ["Whopperflower"]
  },
  "hilichurl-horns": {
    "weapons": [
      "apprentice's-notes",
      "aquila-favonia",
      "cool-steel",
      "dull-blade",
      "favonius-codex",
      "favonius-sword",
      "ferrous-shadow",
      "festering-desire",
      "magic-guide",
      "pocket-grimoire",
      "raven-bow",
      "royal-grimoire",
      "royal-longsword",
      "silver-sword",
      "snow-tombed-starsilver",
      "the-bell",
      "the-stringless",
      "the-viridescent-hunt"
    ],
    "items": [
      {
        "id": "heavy-horn",
        "name": "Heavy Horn",
        "rarity": 2
      },
      {
        "id": "black-bronze-horn",
        "name": "Black Bronze Horn",
        "rarity": 3
      },
      {
        "id": "black-crystal-horn",
        "name": "Black Crystal Horn",
        "rarity": 4
      }
    ],
    "sources": ["Mitachurl", "Lawachurl"]
  },
  "ley-line": {
    "weapons": [
      "alley-hunter",
      "bloodtainted-greatsword",
      "deathmatch",
      "dragonspine-spear",
      "harbinger-of-dawn",
      "hunter's-bow",
      "old-merc's-pal",
      "sacrificial-bow",
      "sacrificial-greatsword",
      "seasoned-hunter's-bow",
      "sharpshooter's-oath",
      "skyward-atlas",
      "skyward-blade",
      "skyward-harp",
      "skyward-pride",
      "sword-of-descension",
      "the-black-sword",
      "the-flute",
      "the-widsith",
      "thrilling-tales-of-dragon-slayers",
      "waster-greatsword",
      "wine-and-song"
    ],
    "items": [
      {
        "id": "dead-ley-line-branch",
        "name": "Dead Ley Line Branch",
        "rarity": 2
      },
      {
        "id": "dead-ley-line-leaves",
        "name": "Dead Ley Line Leaves",
        "rarity": 3
      },
      {
        "id": "ley-line-sprout",
        "name": "Ley Line Sprout",
        "rarity": 4
      }
    ],
    "sources": ["Abyss Mage"]
  },
  "bone-shards": {
    "weapons": [
      "black-tassel",
      "compound-bow",
      "iron-sting",
      "mappa-mare",
      "memory-of-dust",
      "prototype-archaic",
      "prototype-starglitter",
      "serpent-spine",
      "skyrider-greatsword",
      "skyrider-sword",
      "vortex-vanquisher"
    ],
    "items": [
      {
        "id": "fragile-bone-shard",
        "name": "Fragile Bone Shard",
        "rarity": 2
      },
      {
        "id": "sturdy-bone-shard",
        "name": "Sturdy Bone Shard",
        "rarity": 3
      },
      {
        "id": "fossilized-bone-shard",
        "name": "Fossilized Bone Shard",
        "rarity": 4
      }
    ],
    "sources": ["Geovishap Hatchling"]
  },
  "mist-grass": {
    "weapons": [
      "blackcliff-slasher",
      "debate-club",
      "dragon's-bane",
      "dragonspine-spear",
      "eye-of-perception",
      "fillet-blade",
      "halberd",
      "messenger",
      "prototype-amber",
      "prototype-crescent",
      "prototype-rancour",
      "rainslasher",
      "royal-spear",
      "the-unforged",
      "twin-nephrite"
    ],
    "items": [
      {
        "id": "mist-grass-pollen",
        "name": "Mist Grass Pollen",
        "rarity": 2
      },
      {
        "id": "mist-grass",
        "name": "Mist Grass",
        "rarity": 3
      },
      {
        "id": "mist-grass-wick",
        "name": "Mist Grass Wick",
        "rarity": 4
      }
    ],
    "sources": ["Fatui Cicin Mage"]
  },
  "fatui-knives": {
    "weapons": [
      "blackcliff-agate",
      "blackcliff-longsword",
      "blackcliff-warbow",
      "crescent-pike",
      "dark-iron-sword",
      "emerald-orb",
      "lion's-roar",
      "primordial-jade-winged-spear",
      "rust",
      "slingshot",
      "solar-pearl",
      "summit-shaper",
      "white-tassel",
      "whiteblind"
    ],
    "items": [
      {
        "id": "hunter's-sacrificial-knife",
        "name": "Hunter's Sacrificial Knife",
        "rarity": 2
      },
      {
        "id": "agent's-sacrificial-knife",
        "name": "Agent's Sacrificial Knife",
        "rarity": 3
      },
      {
        "id": "inspector's-sacrificial-knife",
        "name": "Inspector's Sacrificial Knife",
        "rarity": 4
      }
    ],
    "sources": ["Fatui Agent"]
  },
  "chaos-parts": {
    "weapons": [
      "amos'-bow",
      "beginner's-protector",
      "favonius-greatsword",
      "favonius-lance",
      "favonius-warbow",
      "frostbearer",
      "iron-point",
      "lost-prayer-to-the-sacred-winds",
      "otherworldly-story",
      "recurve-bow",
      "royal-bow",
      "royal-greatsword",
      "sacrificial-fragments",
      "sacrificial-sword",
      "skyward-spine",
      "traveler's-handy-sword",
      "white-iron-greatsword",
      "wolf's-gravestone"
    ],
    "items": [
      {
        "id": "chaos-device",
        "name": "Chaos Device",
        "rarity": 2
      },
      {
        "id": "chaos-circuit",
        "name": "Chaos Circuit",
        "rarity": 3
      },
      {
        "id": "chaos-core",
        "name": "Chaos Core",
        "rarity": 4
      }
    ],
    "sources": ["Ruin Guard", "Ruin Hunter", "Ruin Grader"]
  }
}

# element gems

gems = {
  "anemo": {
    "sliver": {
      "id": "vayuda-turquoise-sliver",
      "name": "Vayuda Turquoise Sliver",
      "sources": [
        "Anemo Hypostasis",
        "Wolf of the North",
        "Souvenir shop",
        "Memories: Storming Terror I",
        "Memories: Storming Terror II",
        "Memories: Storming Terror III",
        "Memories: Storming Terror IV",
        "Memories: Storming Terror V",
        "Memories: Storming Terror VI",
        "Memories: The Golden Shadow I",
        "Memories: The Golden Shadow II",
        "Memories: The Golden Shadow III",
        "Memories: The Golden Shadow IV"
      ],
      "rarity": 2
    },
    "fragment": {
      "id": "vayuda-turquoise-fragment",
      "name": "Vayuda Turquoise Fragment",
      "sources": [
        "Anemo Hypostasis",
        "Wolf of the North",
        "Memories: Storming Terror I",
        "Memories: Storming Terror II",
        "Memories: Storming Terror III",
        "Memories: Storming Terror IV",
        "Memories: Storming Terror V",
        "Memories: Storming Terror VI",
        "Memories: The Golden Shadow I",
        "Memories: The Golden Shadow II",
        "Memories: The Golden Shadow III",
        "Memories: The Golden Shadow IV"
      ],
      "rarity": 3
    },
    "chunk": {
      "id": "vayuda-turquoise-chunk",
      "name": "Vayuda Turquoise Chunk",
      "sources": [
        "Anemo Hypostasis",
        "Wolf of the North",
        "Memories: Storming Terror III",
        "Memories: Storming Terror IV",
        "Memories: Storming Terror V",
        "Memories: Storming Terror VI",
        "Memories: The Golden Shadow I",
        "Memories: The Golden Shadow II",
        "Memories: The Golden Shadow III",
        "Memories: The Golden Shadow IV"
      ],
      "rarity": 4
    },
    "gemstone": {
      "id": "vayuda-turquoise-gemstone",
      "name": "Vayuda Turquoise Gemstone",
      "sources": [
        "Anemo Hypostasis",
        "Wolf of the North",
        "Memories: Storming Terror V",
        "Memories: Storming Terror VI",
        "Memories: The Golden Shadow III",
        "Memories: The Golden Shadow IV"
      ],
      "rarity": 5
    }
  },
  "cryo": {
    "sliver": {
      "id": "shivada-jade-sliver",
      "name": "Shivada Jade Sliver",
      "sources": [
        "Cryo Regisvine",
        "Wolf of the North",
        "Souvenir shop",
        "Memories: Storming Terror I",
        "Memories: Storming Terror II",
        "Memories: Storming Terror III",
        "Memories: Storming Terror IV",
        "Memories: Storming Terror V",
        "Memories: Storming Terror VI",
        "Memories: The Golden Shadow I",
        "Memories: The Golden Shadow II",
        "Memories: The Golden Shadow III",
        "Memories: The Golden Shadow IV"
      ],
      "rarity": 2
    },
    "fragment": {
      "id": "shivada-jade-fragment",
      "name": "Shivada Jade Fragment",
      "sources": [
        "Cryo Regisvine",
        "Wolf of the North",
        "Memories: Storming Terror I",
        "Memories: Storming Terror II",
        "Memories: Storming Terror III",
        "Memories: Storming Terror IV",
        "Memories: Storming Terror V",
        "Memories: Storming Terror VI",
        "Memories: The Golden Shadow I",
        "Memories: The Golden Shadow II",
        "Memories: The Golden Shadow III",
        "Memories: The Golden Shadow IV"
      ],
      "rarity": 3
    },
    "chunk": {
      "id": "shivada-jade-chunk",
      "name": "Shivada Jade Chunk",
      "sources": [
        "Cryo Regisvine",
        "Wolf of the North",
        "Memories: Storming Terror III",
        "Memories: Storming Terror IV",
        "Memories: Storming Terror V",
        "Memories: Storming Terror VI",
        "Memories: The Golden Shadow I",
        "Memories: The Golden Shadow II",
        "Memories: The Golden Shadow III",
        "Memories: The Golden Shadow IV"
      ],
      "rarity": 4
    },
    "gemstone": {
      "id": "shivada-jade-gemstone",
      "name": "Shivada Jade Gemstone",
      "sources": [
        "Cryo Regisvine",
        "Wolf of the North",
        "Memories: Storming Terror V",
        "Memories: Storming Terror VI",
        "Memories: The Golden Shadow III",
        "Memories: The Golden Shadow IV"
      ],
      "rarity": 5
    }
  },
  "electro": {
    "sliver": {
      "id": "vajrada-amethyst-sliver",
      "name": "Vajrada Amethyst Sliver",
      "sources": [
        "Electro Hypostasis",
        "Wolf of the North",
        "Souvenir shop",
        "Memories: Storming Terror I",
        "Memories: Storming Terror II",
        "Memories: Storming Terror III",
        "Memories: Storming Terror IV",
        "Memories: Storming Terror V",
        "Memories: Storming Terror VI",
        "Memories: The Golden Shadow I",
        "Memories: The Golden Shadow II",
        "Memories: The Golden Shadow III",
        "Memories: The Golden Shadow IV"
      ],
      "rarity": 2
    },
    "fragment": {
      "id": "vajrada-amethyst-fragment",
      "name": "Vajrada Amethyst Fragment",
      "sources": [
        "Electro Hypostasis",
        "Wolf of the North",
        "Memories: Storming Terror I",
        "Memories: Storming Terror II",
        "Memories: Storming Terror III",
        "Memories: Storming Terror IV",
        "Memories: Storming Terror V",
        "Memories: Storming Terror VI",
        "Memories: The Golden Shadow I",
        "Memories: The Golden Shadow II",
        "Memories: The Golden Shadow III",
        "Memories: The Golden Shadow IV"
      ],
      "rarity": 3
    },
    "chunk": {
      "id": "vajrada-amethyst-chunk",
      "name": "Vajrada Amethyst Chunk",
      "sources": [
        "Electro Hypostasis",
        "Wolf of the North",
        "Memories: Storming Terror III",
        "Memories: Storming Terror IV",
        "Memories: Storming Terror V",
        "Memories: Storming Terror VI",
        "Memories: The Golden Shadow I",
        "Memories: The Golden Shadow II",
        "Memories: The Golden Shadow III",
        "Memories: The Golden Shadow IV"
      ],
      "rarity": 4
    },
    "gemstone": {
      "id": "vajrada-amethyst-gemstone",
      "name": "Vajrada Amethyst Gemstone",
      "sources": [
        "Electro Hypostasis",
        "Wolf of the North",
        "Memories: Storming Terror V",
        "Memories: Storming Terror VI",
        "Memories: The Golden Shadow III",
        "Memories: The Golden Shadow IV"
      ],
      "rarity": 5
    }
  },
  "geo": {
    "sliver": {
      "id": "prithiva-topaz-sliver",
      "name": "Prithiva Topaz Sliver",
      "sources": [
        "Geo Hypostasis",
        "Wolf of the North",
        "Souvenir shop",
        "Memories: Storming Terror I",
        "Memories: Storming Terror II",
        "Memories: Storming Terror III",
        "Memories: Storming Terror IV",
        "Memories: Storming Terror V",
        "Memories: Storming Terror VI",
        "Memories: The Golden Shadow I",
        "Memories: The Golden Shadow II",
        "Memories: The Golden Shadow III",
        "Memories: The Golden Shadow IV"
      ],
      "rarity": 2
    },
    "fragment": {
      "id": "prithiva-topaz-fragment",
      "name": "Prithiva Topaz Fragment",
      "sources": [
        "Geo Hypostasis",
        "Wolf of the North",
        "Memories: Storming Terror I",
        "Memories: Storming Terror II",
        "Memories: Storming Terror III",
        "Memories: Storming Terror IV",
        "Memories: Storming Terror V",
        "Memories: Storming Terror VI",
        "Memories: The Golden Shadow I",
        "Memories: The Golden Shadow II",
        "Memories: The Golden Shadow III",
        "Memories: The Golden Shadow IV"
      ],
      "rarity": 3
    },
    "chunk": {
      "id": "prithiva-topaz-chunk",
      "name": "Prithiva Topaz Chunk",
      "sources": [
        "Geo Hypostasis",
        "Wolf of the North",
        "Memories: Storming Terror III",
        "Memories: Storming Terror IV",
        "Memories: Storming Terror V",
        "Memories: Storming Terror VI",
        "Memories: The Golden Shadow I",
        "Memories: The Golden Shadow II",
        "Memories: The Golden Shadow III",
        "Memories: The Golden Shadow IV"
      ],
      "rarity": 4
    },
    "gemstone": {
      "id": "prithiva-topaz-gemstone",
      "name": "Prithiva Topaz Gemstone",
      "sources": [
        "Geo Hypostasis",
        "Wolf of the North",
        "Memories: Storming Terror V",
        "Memories: Storming Terror VI",
        "Memories: The Golden Shadow III",
        "Memories: The Golden Shadow IV"
      ],
      "rarity": 5
    }
  },
  "hydro": {
    "sliver": {
      "id": "varunada-lazurite-sliver",
      "name": "Varunada Lazurite Sliver",
      "sources": [
        "Oceanid",
        "Wolf of the North",
        "Souvenir shop",
        "Memories: Storming Terror I",
        "Memories: Storming Terror II",
        "Memories: Storming Terror III",
        "Memories: Storming Terror IV",
        "Memories: Storming Terror V",
        "Memories: Storming Terror VI",
        "Memories: The Golden Shadow I",
        "Memories: The Golden Shadow II",
        "Memories: The Golden Shadow III",
        "Memories: The Golden Shadow IV"
      ],
      "rarity": 2
    },
    "fragment": {
      "id": "varunada-lazurite-fragment",
      "name": "Varunada Lazurite Fragment",
      "sources": [
        "Oceanid",
        "Wolf of the North",
        "Memories: Storming Terror I",
        "Memories: Storming Terror II",
        "Memories: Storming Terror III",
        "Memories: Storming Terror IV",
        "Memories: Storming Terror V",
        "Memories: Storming Terror VI",
        "Memories: The Golden Shadow I",
        "Memories: The Golden Shadow II",
        "Memories: The Golden Shadow III",
        "Memories: The Golden Shadow IV"
      ],
      "rarity": 3
    },
    "chunk": {
      "id": "varunada-lazurite-chunk",
      "name": "Varunada Lazurite Chunk",
      "sources": [
        "Oceanid",
        "Wolf of the North",
        "Memories: Storming Terror III",
        "Memories: Storming Terror IV",
        "Memories: Storming Terror V",
        "Memories: Storming Terror VI",
        "Memories: The Golden Shadow I",
        "Memories: The Golden Shadow II",
        "Memories: The Golden Shadow III",
        "Memories: The Golden Shadow IV"
      ],
      "rarity": 4
    },
    "gemstone": {
      "id": "varunada-lazurite-gemstone",
      "name": "Varunada Lazurite Gemstone",
      "sources": [
        "Oceanid",
        "Wolf of the North",
        "Memories: Storming Terror V",
        "Memories: Storming Terror VI",
        "Memories: The Golden Shadow III",
        "Memories: The Golden Shadow IV"
      ],
      "rarity": 5
    }
  },
  "pyro": {
    "sliver": {
      "id": "agnidus-agate-sliver",
      "name": "Agnidus Agate Sliver",
      "sources": [
        "Pyro Regisvine",
        "Wolf of the North",
        "Souvenir shop",
        "Memories: Storming Terror I",
        "Memories: Storming Terror II",
        "Memories: Storming Terror III",
        "Memories: Storming Terror IV",
        "Memories: Storming Terror V",
        "Memories: Storming Terror VI",
        "Memories: The Golden Shadow I",
        "Memories: The Golden Shadow II",
        "Memories: The Golden Shadow III",
        "Memories: The Golden Shadow IV"
      ],
      "rarity": 2
    },
    "fragment": {
      "id": "agnidus-agate-fragment",
      "name": "Agnidus Agate Fragment",
      "sources": [
        "Pyro Regisvine",
        "Wolf of the North",
        "Memories: Storming Terror I",
        "Memories: Storming Terror II",
        "Memories: Storming Terror III",
        "Memories: Storming Terror IV",
        "Memories: Storming Terror V",
        "Memories: Storming Terror VI",
        "Memories: The Golden Shadow I",
        "Memories: The Golden Shadow II",
        "Memories: The Golden Shadow III",
        "Memories: The Golden Shadow IV"
      ],
      "rarity": 3
    },
    "chunk": {
      "id": "agnidus-agate-chunk",
      "name": "Agnidus Agate Chunk",
      "sources": [
        "Pyro Regisvine",
        "Wolf of the North",
        "Memories: Storming Terror III",
        "Memories: Storming Terror IV",
        "Memories: Storming Terror V",
        "Memories: Storming Terror VI",
        "Memories: The Golden Shadow I",
        "Memories: The Golden Shadow II",
        "Memories: The Golden Shadow III",
        "Memories: The Golden Shadow IV"
      ],
      "rarity": 4
    },
    "gemstone": {
      "id": "agnidus-agate-gemstone",
      "name": "Agnidus Agate Gemstone",
      "sources": [
        "Pyro Regisvine",
        "Wolf of the North",
        "Memories: Storming Terror V",
        "Memories: Storming Terror VI",
        "Memories: The Golden Shadow III",
        "Memories: The Golden Shadow IV"
      ],
      "rarity": 5
    }
  },
  "traveler": {
    "sliver": {
      "id": "brilliant-diamond-sliver",
      "name": "Brilliant Diamond Sliver",
      "sources": ["Adventure Rank 15"],
      "rarity": 2
    },
    "fragment": {
      "id": "brilliant-diamond-fragment",
      "name": "Brilliant Diamond Fragment",
      "sources": [
        "Adventure Rank 25",
        "Adventure Rank 26",
        "Adventure Rank 28",
        "Adventure Rank 30"
      ],
      "rarity": 3
    },
    "chunk": {
      "id": "brilliant-diamond-chunk",
      "name": "Brilliant Diamond Chunk",
      "sources": [
        "Adventure Rank 35",
        "Adventure Rank 38",
        "Adventure Rank 40"
      ],
      "rarity": 4
    },
    "gemstone": {
      "id": "brilliant-diamond-gemstone",
      "name": "Brilliant Diamond Gemstone",
      "sources": [
        "Adventure Rank 41",
        "Adventure Rank 42",
        "Adventure Rank 45"
      ],
      "rarity": 5
    }
  }
}

# local specialties

local = {
  "mondstadt": [
    {
      "id": "calla-lily",
      "name": "Calla Lily",
      "characters": ["diona", "kaeya"]
    },
    {
      "id": "cecilia",
      "name": "Cecilia",
      "characters": ["albedo", "venti"]
    },
    {
      "id": "dandelion-seed",
      "name": "Dandelion Seed",
      "characters": ["jean"]
    },
    {
      "id": "philanemo-mushroom",
      "name": "Philanemo Mushroom",
      "characters": ["barbara", "klee", "mona"]
    },
    {
      "id": "small-lamp-grass",
      "name": "Small Lamp Grass",
      "characters": ["amber", "diluc", "fischl"]
    },
    {
      "id": "valberry",
      "name": "Valberry",
      "characters": ["lisa", "noelle"]
    },
    {
      "id": "windwheel-aster",
      "name": "Windwheel Aster",
      "characters": ["bennett", "sucrose", "traveler-anemo", "traveler-geo"]
    },
    {
      "id": "wolfhook",
      "name": "Wolfhook",
      "characters": ["razor"]
    }
  ],
  "liyue": [
    {
      "id": "cor-lapis",
      "name": "Cor Lapis",
      "characters": ["chongyun", "keqing", "zhongli"]
    },
    {
      "id": "glaze-lily",
      "name": "Glaze Lily",
      "characters": ["ningguang"]
    },
    {
      "id": "jueyun-chili",
      "name": "Jueyun Chili",
      "characters": ["xiangling"]
    },
    {
      "id": "noctilucous-jade",
      "name": "Noctilucous Jade",
      "characters": ["beidou"]
    },
    {
      "id": "qingxin",
      "name": "Qingxin",
      "characters": ["ganyu", "xiao"]
    },
    {
      "id": "silk-flower",
      "name": "Silk Flower",
      "characters": ["xingqiu"]
    },
    {
      "id": "starconch",
      "name": "Starconch",
      "characters": ["tartaglia"]
    },
    {
      "id": "violetgrass",
      "name": "Violetgrass",
      "characters": ["qiqi", "xinyan"]
    }
  ]
}