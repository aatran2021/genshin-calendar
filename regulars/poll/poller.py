import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "regulars_project.settings")
django.setup()

from regulars_rest.models import BossItem

def poll():
	while True:
		print('Regulars poller polling for data')
		try:
			response = requests.get("https://api.genshin.dev/materials/boss-material")
			content = json.loads(response.content)
			for item in content:
				BossItem.objects.update_or_create(
					name= content[item]["name"],
					boss = content[item]["source"],
					defaults={
					"characters": content[item]["characters"]
					}
				)
			print("success")
		except Exception as e:
			print(e, file=sys.stderr)
		time.sleep(60)


if __name__ == "__main__":
		poll()