import { BrowserRouter, Routes, Route } from "react-router-dom";
import Nav from './components/Nav';
import ReactCalendar from "./components/ReactCalendar";
import Characters from "./components/Characters";

function App() {

  return (
    <>
      <BrowserRouter>
        <Nav />
        <div className="container">
          <Routes>
            <Route path="/" element={<ReactCalendar />} />
            <Route path="/characters" element={<Characters />} />
          </Routes>
        </div>
      </BrowserRouter>
    </>
  )
}

export default App
