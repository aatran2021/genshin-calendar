import React, { useState, useEffect } from "react";
import AddCharacters from "./AddCharacter";

export default function Characters() {
    const [characters, setCharacters] = useState([]);

    const fetchCharacterData = async () => {
        const url = "http://localhost:8100/api/characters/";

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            console.log(data.characters)
            setCharacters(data.characters)
        }
    }

    useEffect(() => {
        fetchCharacterData();
    }, []);

    return (
        <div className="position-relative mt-5">
            <AddCharacters characters={characters} />
            <div className="position-absolute start-50 translate-middle-x" style={{width: "60vw", display: "flex"}}>
                <div style={{width: "2vw", minWidth: "40px", backgroundColor: "gray"}}>
                    <div type="button" className="btn btn-primary" data-bs-toggle="modal" data-bs-target="#addCharacters">+</div>
                </div>
                <div style={{width: "2vw"}}></div>
                <div className="container" style={{width: "60vw"}}>
                    <div className="card mb-3">
                        <div className="row g-0">
                            <div className="col-md-2">
                                <img src="https://api.genshin.dev/characters/albedo/icon/"/>
                            </div>
                            <div className="col-md-10">
                                <div className="card-body">
                                    <h5 className="card-title">Card Title</h5>
                                    <p className="card-text">Card Text</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="card mb-3">
                        <div className="row g-0">
                            <div className="col-md-2">
                                <img src="https://api.genshin.dev/characters/albedo/icon/"/>
                            </div>
                            <div className="col-md-10">
                                <div className="card-body">
                                    <h5 className="card-title">Card Title</h5>
                                    <p className="card-text">Card Text</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="card mb-3">
                        <div className="row g-0">
                            <div className="col-md-2">
                                <img src="https://api.genshin.dev/characters/albedo/icon/"/>
                            </div>
                            <div className="col-md-10">
                                <div className="card-body">
                                    <h5 className="card-title">Card Title</h5>
                                    <p className="card-text">Card Text</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
