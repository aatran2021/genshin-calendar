import React, { useState, useEffect } from "react";

export default function AddCharacters({ characters }) {
    return(
        <div className="modal fade" id="addCharacters" tabIndex="-1" aria-labelledby="addCharactersLabel" aria-hidden="true">
            <div className="modal-dialog modal-xl">
                <div className="modal-content">
                    <div className="modal-header">
                        <h1 className="modal-title fs-5" id="addCharactersLabel">Add Characters</h1>
                        <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div className="modal-body">
                        <div className="container">
                            <div className="row">
                                <div className="col-8">
                                    <div className="row">
                                        {characters.map(character => {
                                            return (
                                                <div className="card m-2" style={{width: "125px"}}>
                                                    <img className="card-img-top" src={character.icon} />
                                                    <div className="card-body">
                                                        <p className="card-text">{character.name}</p>
                                                    </div>
                                                </div>
                                            )
                                        })}
                                    </div>
                                </div>
                                <div className="col-4">
                                        <div className="card m-2" style={{width: "300px"}}>
                                            <img className="card-img-top" src="..." />
                                            <div className="card-body">
                                                <p className="card-text">"Shopping Cart</p>
                                            </div>
                                        </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="modal-footer">
                        <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">X</button>
                        <button type="button" className="btn btn-primary">Add</button>
                    </div>
                </div>
            </div>
        </div>
    )
}
