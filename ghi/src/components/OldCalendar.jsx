import { AiOutlineLeft, AiOutlineRight } from "react-icons/ai";
import React, { useState, useEffect } from "react";
import "../css/calendar.css";

export default function Calendar() {
    const months = [
        "January",
        "February",
        "March",
        "April",
        "May",
        "June",
        "July",
        "August",
        "September",
        "October",
        "November",
        "December",
      ];

    const [month, setMonth] = useState(months[(new Date()).getMonth()])
    const [year, setYear] = useState((new Date()).getFullYear())
    const [currentDate, setCurrentDate] = useState(new Date());

    // const currentDate = new Date();

    function renderCalendar() {
        // const months = [
        //   "January",
        //   "February",
        //   "March",
        //   "April",
        //   "May",
        //   "June",
        //   "July",
        //   "August",
        //   "September",
        //   "October",
        //   "November",
        //   "December",
        // ];

        const lastDay = new Date(
            currentDate.getFullYear(),
            currentDate.getMonth() + 1,
            0
        ).getDate();

        const firstWeekday = new Date(
            currentDate.getFullYear(),
            currentDate.getMonth(),
            1
        ).getDay();

        const prevLastDay = new Date(
            currentDate.getFullYear(),
            currentDate.getMonth(),
            0
        ).getDate();

        const lastWeekday = new Date(
            currentDate.getFullYear(),
            currentDate.getMonth() + 1,
            0
        ).getDay();

        // Need to change to implement s
        // document.querySelector(".month").innerHTML =
        //     months[currentDate.getMonth()] + " " + currentDate.getFullYear();

        let days = "";

        for (let i = firstWeekday; i > 0; i--) {
            days += `<div class="prevMonthDates">${prevLastDay - i + 1}</div>`;
        }

        for (let j = 1; j <= lastDay; j++) {
            if (
                j === currentDate.getDate() &&
                currentDate.getMonth() === new Date().getMonth()
            ) {
                days += `<div class="currentDate">${j}</div>`;
            } else {
                days += `<div>${j}</div>`;
            }
        }
        for (let k = 1; k <= 6 - lastWeekday; k++) {
            days += `<div class="nextMonthDates">${k}</div>`;
        }
        document.querySelector(".days").innerHTML = days;
    }

    useEffect(() => {
        renderCalendar();
    });

    const goPrevMonth = () => {
        currentDate.setMonth(currentDate.getMonth() - 1);

        renderCalendar();
    }

    const goNextMonth = () => {
        currentDate.setMonth(currentDate.getMonth() + 1);
        renderCalendar();
    }

    return (
        <>
            <div className="wholePage">
                <div className="col" id="mainCalendar">
                    <div className="containerCalendar">
                        <div className="headerCalendar">
                            <AiOutlineLeft id="goPrevMonth" onClick={goPrevMonth} />
                        <div className="month">{month} {year}</div>
                            <AiOutlineRight id="goNextMonth" onClick={goNextMonth} />
                        </div>
                        <div className="weekdays">
                            <div>Sun</div>
                            <div>Mon</div>
                            <div>Tue</div>
                            <div>Wed</div>
                            <div>Thu</div>
                            <div>Fri</div>
                            <div>Sat</div>
                        </div>
                        <div className="days"></div>
                    </div>
                </div>
            </div>
        </>
    )
}
