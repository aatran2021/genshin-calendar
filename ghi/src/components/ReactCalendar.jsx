import { AiOutlineLeft, AiOutlineRight } from "react-icons/ai";
import React, { useState, useEffect } from "react";
import "../css/calendar.css";

export default function ReactCalendar() {
    const [currentDate, setCurrentDate] = useState(new Date());
    const [month, setMonth] = useState((new Date()).toLocaleString("default", {month: "long"}));
    const [year, setYear] = useState((new Date()).toLocaleString("default", {year: "numeric"}))
    const [prevMonthDays, setPrevMonthDays] = useState([]);
    const [currentMonthDays, setCurrentMonthDays] = useState([]);
    const [futureMonthDays, setFutureMonthDays] = useState([]);

    const renderCalendar = () => {
         // .getDate() returns day of month (i.e. May 7th -> 7)
        // Returns the last day of the current month
        const lastDay = new Date(
            currentDate.getFullYear(),
            currentDate.getMonth() + 1,
            0
        ).getDate();

        // .getDay() returns 0-6 based on Sunday-Saturday
        // Returns the first weekday of the current month
        const firstWeekday = new Date(
            currentDate.getFullYear(),
            currentDate.getMonth(),
            1
        ).getDay();

        // Returns the last day of the previous month
        const prevLastDay = new Date(
            currentDate.getFullYear(),
            currentDate.getMonth(),
            0
        ).getDate();

        // Returns the last weekday of the previous month
        const lastWeekday = new Date(
            currentDate.getFullYear(),
            currentDate.getMonth() + 1,
            0
        ).getDay();

        let prevMonthDays = [];
        let currentMonthDays = [];
        let futureMonthDays = [];

        for (let i = firstWeekday; i > 0; i--) {
            prevMonthDays.push(prevLastDay -i + 1);
        }

        setPrevMonthDays(prevMonthDays)

        for (let j = 1; j <= lastDay; j++) {
            currentMonthDays.push(j);
        }

        setCurrentMonthDays(currentMonthDays)

        for (let k = 1; k <= 6 - lastWeekday; k++) {
            futureMonthDays.push(k);
        }

        setFutureMonthDays(futureMonthDays)
    }



    useEffect(() => {
        renderCalendar();
    }, [currentDate])

    const goPrevMonth = () => {
        // setMonth(new Date(currentDate.getFullYear(), currentDate.getMonth() - 1).toLocaleString("default", {month: "long"}))
        // setYear(new Date(currentDate.getFullYear(), currentDate.getMonth() - 1).toLocaleString("default", {year: "numeric"}))
        setCurrentDate(currentDate => {
            const newDate = new Date(currentDate.getFullYear(), currentDate.getMonth() - 1)
            setMonth(newDate.toLocaleString("default", {month: "long"}))
            setYear(newDate.toLocaleString("default", {year: "numeric"}))
            return newDate
        });
    }

    const goNextMonth = () => {
        // setMonth(new Date(currentDate.getFullYear(), currentDate.getMonth() + 1).toLocaleString("default", {month: "long"}))
        // setYear(new Date(currentDate.getFullYear(), currentDate.getMonth() + 1).toLocaleString("default", {year: "numeric"}))
        setCurrentDate(currentDate => {
            const newDate = new Date(currentDate.getFullYear(), currentDate.getMonth() + 1)
            setMonth(newDate.toLocaleString("default", {month: "long"}))
            setYear(newDate.toLocaleString("default", {year: "numeric"}))
            return newDate
        });
    }



    return (
        <>
            <div className="wholePage">
                <div className="col" id="mainCalendar">
                    <div className="containerCalendar">
                        <div className="headerCalendar">
                            <AiOutlineLeft id="goPrevMonth" onClick={goPrevMonth} />
                        <div className="month">{month} {year}</div>
                            <AiOutlineRight id="goNextMonth" onClick={goNextMonth} />
                        </div>
                        <div className="weekdays">
                            <div>Sun</div>
                            <div>Mon</div>
                            <div>Tue</div>
                            <div>Wed</div>
                            <div>Thu</div>
                            <div>Fri</div>
                            <div>Sat</div>
                        </div>
                        <div className="days">
                            {prevMonthDays.map(day => {
                                return (
                                    <div className="prevMonthDates" key={day}>{day}</div>
                                )
                            })}
                            {currentMonthDays.map(day => {
                                return (
                                    day === currentDate.getDate() &&
                                    currentDate.getMonth() === new Date().getMonth() ?
                                    <div className="currentDate" key={day}>{day}</div> :
                                    <div key={day}>{day}</div>
                                )
                            })}
                            {futureMonthDays.map(day => {
                                return (
                                    <div className="nextMonthDates" key={day}>{day}</div>
                                )
                            })}
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}
