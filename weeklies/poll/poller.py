import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "weeklies_project.settings")
django.setup()

from weeklies_rest.models import WeeklyItem
from weeklies_rest.models import WeeklyBoss
from weeklies_rest.weekliesdatabase import bossnames

def get_boss_name(name):
	for boss in bossnames:
		if name in bossnames[boss]:
			return boss

def poll():
	while True:
		print('Weeklies poller polling for data')
		try:
			response = requests.get("https://api.genshin.dev/materials/talent-boss")
			content = json.loads(response.content)
			for boss in bossnames:
				WeeklyBoss.objects.update_or_create(
					name= boss
				)
			for item in content:
				print(item)	
				WeeklyItem.objects.update_or_create(
					name= content[item]["name"],
					boss= WeeklyBoss.objects.get(name=get_boss_name(content[item]["name"])),
					defaults={
					"characters": content[item]["characters"]
					}
				)
			print("success")
		except Exception as e:
			print(e, file=sys.stderr)
		time.sleep(60)


if __name__ == "__main__":
		poll()