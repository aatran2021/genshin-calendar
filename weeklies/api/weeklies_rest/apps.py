from django.apps import AppConfig


class WeekliesRestConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'weeklies_rest'
