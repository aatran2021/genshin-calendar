# Use Poller to grab data from Genshin API
# Boss names are used to populate WeeklyBoss models for WeeklyItem models to ref as foreign keys

bossnames = {
    "stormterror": ["Dvalin's Plume", "Dvalin's Claw", "Dvalin's Sigh"],
    "boreas": ["Tail of Boreas", "Ring of Boreas", "Spirit Locket of Boreas"],
    "childe": ["Tusk of Monoceros Caeli", "Shard of a Foul Legacy", "Shadow of the Warrior"],
    "azhdaha": ["Bloodjade Branch", "Dragon Lord's Crown", "Gilded Scale"],
    "signora": ["Molten Moment", "Hellfire Butterfly", "Ashen Heart"]
}

# gems = {
#     "anemo": [],
#     "electro": [],
#     "hydro": [],
#     "cryo": [],
#     "pyro": [],
#     "geo": [],
#     "dendro": [],
# }

weeklydatabase = {
    "Stormterror": {
        "material": ["Dvalin's Plume", "Dvalin's Claw", "Dvalin's Sigh"],
        "gems": ["anemo", "electro", "hydro"]
        },
    "Boreas": {
        "material": ["Tail of Boreas", "Ring of Boreas", "Spirit Locket of Boreas"],
        "gems": ["cryo", "geo", "pyro"]
        },
    "Childe": {
        "material": ["Tusk of Monoceros Caeli", "Shard of a Foul Legacy", "Shadow of the Warrior"],
        "gems": ["hydro", "electro", "cryo"]
        },
    "Azhdaha": {
        "material": ["Bloodjade Branch", "Dragon Lord's Crown", "Gilded Scale"],
        "gems": ["geo", "hydro", "electro", "cryo", "pyro"]
        },
    "Signora": {
        "material": ["Molten Moment", "Hellfire Butterfly", "Ashen Heart"],
        "gems": ["cryo", "pyro"]
        },
    "Raiden": {
        "material": ["Mudra of the Malefic General", "Tears of the Calamitous God", "The Meaning of Aeons"],
        "gems": ["electro"]
        },
    "Shouki": {
        "material": ["Puppet Strings", "Mirror of Mushin", "Daka's Bell"],
        "gems": ["anemo", "electro", "hydro"]
        }
}
