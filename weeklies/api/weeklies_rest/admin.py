from django.contrib import admin
from .models import ( WeeklyItem, WeeklyBoss )


admin.site.register(WeeklyItem)
admin.site.register(WeeklyBoss)