from django.db import models
from django.contrib.postgres.fields import ArrayField
from django.urls import reverse

# Create your models here.

class WeeklyBoss(models.Model):
    # name of the boss
    name = models.CharField(max_length=150)

    def __str__(self):
        return self.name

class WeeklyItem(models.Model):
    # name of the item
    name = models.CharField(max_length=150) 
    # list of characters that need the item
    characters = ArrayField(models.CharField(max_length=150))
    # name of the boss that drops this item
    boss = models.ForeignKey(
        WeeklyBoss,
        related_name="weeklyitem",
        on_delete=models.CASCADE
    )

    def __str__(self):
        return self.name
    
    def get_api_url(self):
        return reverse("api_weeklyitem", kwargs={"pk": self.id})    

